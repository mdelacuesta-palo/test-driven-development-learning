import { expect } from 'chai';
import routes from '.';

describe('controllers', () => {
  const target = routes;
  it('should return a router middleware', () => {
    expect(target).to.be.a('function');
  });
});
