import Router from 'koa-router';
import ping from './ping';
import version from './version';

const router = new Router({
  prefix: ''
});

router.get('/version', version)
  .get('/ping', ping);

export default router;
