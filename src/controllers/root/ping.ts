import type { Context } from 'koa';

const ping = async (context: Context) => {
  context.status = 200;
  context.body = {
    message: 'It works!'
  };
};

export default ping;
