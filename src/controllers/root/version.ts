import type { Context } from 'koa';

const version = async (context: Context) => {
  context.status = 200;
  context.body = {
    version: '1.0.0'
  };
};

export default version;
