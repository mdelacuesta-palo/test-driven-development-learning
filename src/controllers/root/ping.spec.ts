import { expect } from 'chai';
import type { Context } from 'koa';
import ping from './ping';

describe('root.ping', () => {
  const target = ping;
  const context = {} as Context;
  it('should have an http status', () => {
    target(context);
    expect(context.status).to.be.a('number');
  });

  it('should return status 200', () => {
    target(context);
    expect(context.status).to.equal(200);
  });

  it('should return a body', () => {
    target(context);
    expect(context.body).to.be.an('object');
  });

  it('should return a message body', () => {
    target(context);
    expect(context.body).to.eql({
      message: 'It works!'
    });
  });
});
