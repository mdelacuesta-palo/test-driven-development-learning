import Router from 'koa-router';

const router = new Router({
  prefix: '/companies'
});

export default router;
