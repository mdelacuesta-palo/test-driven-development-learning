import { expect } from 'chai';
import { Chance } from 'chance';
import type { Context } from 'koa';
import { ObjectId } from 'mongodb';
import sinon, { SinonStub } from 'sinon';
import * as UserService from '../../services/user';
import * as CompanyService from '../../services/company';
import { addNewUserToCompany } from './addNewUserToCompany';

const chance = new Chance();

describe.only('controllers.companies.addNewUserToCompany', () => {
  let context = {} as Context;
  let createUserStub: SinonStub;
  let addUserToCompanyStub: SinonStub;

  beforeEach(() => {
    context = {
      request: {
        body: {
          companyId: '518cbb1389da79d3a25453f9'
        }
      }
    } as Context;
    createUserStub = sinon.stub(UserService, 'createUser');
    addUserToCompanyStub = sinon.stub(CompanyService, 'addUserToCompany');
  });

  afterEach(() => {
    createUserStub.restore();
    addUserToCompanyStub.restore();
  });

  it('should return status 200 - OK if its succesful', async () => {
    // arrange
    const expected = 200; // OK

    // act
    await addNewUserToCompany(context);


    // assert
    expect(expected).to.equal(context.status);
  });

  it('should insert a user record to the database using the body parameters', async () => {
    // arrange
    const objectId = ObjectId.createFromTime(chance.timestamp());
    createUserStub.resolves(objectId);

    const email = 'socheat@palo-it.com';
    const firstName = 'Socheat';
    const lastName = 'Smith';

    context.request.body = {
      ...context.request.body,
      email,
      firstName,
      lastName
    };

    // act
    await addNewUserToCompany(context);

    // assert
    expect(createUserStub.calledOnceWith(sinon.match({
      email,
      firstName,
      lastName
    }))).to.equal(true);
  });

  it('should add the user to the company', async () => {
    // arrange
    const userId = ObjectId.createFromTime(chance.timestamp());
    createUserStub.resolves(userId);

    const companyId = '518cbb1389da79d3a25453f9';
    addUserToCompanyStub.resolves();

    const expectedCompanyId = ObjectId.createFromHexString(companyId);
    const expectedUserId = userId;

    const email = 'socheat@palo-it.com';
    const firstName = 'Socheat';
    const lastName = 'Smith';

    context.request.body = {
      companyId,
      email,
      firstName,
      lastName
    };

    // act
    await addNewUserToCompany(context);

    // assert
    expect(addUserToCompanyStub.calledOnceWithExactly(expectedCompanyId, expectedUserId)).to.equal(true);
  });

  it('should throw an error if company doesnt exist', async () => {

  });
});
