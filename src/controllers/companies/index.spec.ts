import { expect } from 'chai';
import router from '.';

describe('companies.routes', () => {
  const target = router;
  it('should return a router middleware', () => {
    const actual = target.routes();
    expect(actual).to.be.a('function');
  });
});
