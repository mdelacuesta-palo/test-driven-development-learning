import type { Context } from 'koa';
import { ObjectId } from 'mongodb';
import { addUserToCompany } from '../../services/company';
import { createUser } from '../../services/user';

// status 200 - OK
// it should create the user in the database
// it should assign that user to a company

export const addNewUserToCompany = async (context: Context) => {
  const { firstName, lastName, email, companyId } = context.request.body;
  const userObjectId = await createUser({
    firstName,
    lastName,
    email
  });

  const companyObjectId = ObjectId.createFromHexString(companyId);
  await addUserToCompany(companyObjectId, userObjectId);

  context.status = 200;
};
