import { combineRouters } from '../utils/routing';
import companiesRoute from './companies';
import rootRoutes from './root';
import usersRoute from './users';

export default combineRouters([rootRoutes, companiesRoute, usersRoute]);
