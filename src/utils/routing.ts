import compose from 'koa-compose';
import type Router from 'koa-router';

export const combineRouters = (routers: Router[]) => {
  const middleWares: Router.IMiddleware[] = [];
  routers.forEach(r => {
    middleWares.push(r.routes());
    middleWares.push(r.allowedMethods());
  });
  return compose(middleWares);
};
