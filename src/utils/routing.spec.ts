import { expect } from 'chai';
import Router from 'koa-router';
import { combineRouters } from './routing';

describe('routing', () => {
  describe('combineRouters', () => {
    it('should combine routers', () => {
      const router1 = new Router({
        prefix: '/route1'
      });
      const router2 = new Router({
        prefix: '/route2'
      });

      const router = combineRouters([router1, router2]);
      expect(router).to.be.a('function');
    });
  });
});
