import { ObjectId } from 'mongodb';
import { User } from '../models/user';
import { USER_COLLECTION } from '../utils/consts';
import { getCollection } from '../utils/data';

export const createUser = async (user: User): Promise<ObjectId> => {
  const collection = await getCollection(USER_COLLECTION);
  const document = await collection.insertOne(user);
  return document.insertedId;
};
