import { expect } from 'chai';
import Chance from 'chance';
import { Collection, InsertOneResult, ObjectId } from 'mongodb';
import sinon from 'sinon';
import { StubbedInstance, stubInterface } from 'ts-sinon';
import { User } from '../models/user';
import { USER_COLLECTION } from '../utils/consts';
import * as DataHelper from '../utils/data';
import { createUser } from './user';

const chance = new Chance();
let userCollectionStub: StubbedInstance<Collection>;

describe('services.user', () => {
  describe('createUser', () => {
    const target = createUser;

    beforeEach(() => {
      userCollectionStub = stubInterface<Collection>();
      sinon.stub(DataHelper, 'getCollection').withArgs(USER_COLLECTION).resolves(userCollectionStub);
    });

    it('should create a user', async () => {
      // arrange
      const user: User = {
        email: chance.email(),
        firstName: chance.name(),
        lastName: chance.name(),
      };

      const expected = ObjectId.createFromTime(chance.timestamp());
      const insertResult: InsertOneResult<Document> = {
        acknowledged: true,
        insertedId: expected
      };

      userCollectionStub.insertOne.resolves(insertResult);

      // act
      const actual = await target(user);

      // assert
      expect(expected).to.equal(actual);
    });
  });
});
