import type { ObjectId } from 'mongodb';

export interface Identifiable {
  id?: ObjectId;
}
