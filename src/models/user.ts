import { Identifiable } from './core';

export interface User extends Identifiable {
  email: string;
  firstName: string;
  lastName: string;
  middleName?: string;
}
