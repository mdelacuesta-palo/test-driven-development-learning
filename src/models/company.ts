import { ObjectId } from 'mongodb';
import { Identifiable } from './core';

export interface Company extends Identifiable {
  companyName: string;
  users: ObjectId[];
}
