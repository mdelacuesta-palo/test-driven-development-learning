import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import server from './server';

chai.use(chaiHttp);

describe('server', () => {
  let target: ChaiHttp.Agent;
  before(() => {
    target = chai.request(server).keepOpen();
  });

  after(() => {
    target.close();
  });

  it('should host an http app', async () => {
    const response = await target.get('/ping');
    expect(response.body.message).to.equal('It works!');
  });
});
