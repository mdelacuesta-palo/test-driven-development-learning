import { Server } from 'http';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import controllers from './controllers';

const PORT = process.env.PORT || '8000';

const app = new Koa();

app.use(bodyParser());
app.use(controllers);

const server: Server = app.listen(PORT);

export default server;
